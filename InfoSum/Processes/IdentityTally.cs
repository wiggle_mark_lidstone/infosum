﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using InfoSum.Anonymisation;
using InfoSum.DataSources;
using InfoSum.Storage;

namespace InfoSum.Processes
{
    public class IdentityTally
    {
        private const int Status_NotStarted = 0;
        private const int Status_Running = 1;
        private const int Status_Completed = 2;
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        private readonly IDictionary<Guid, DatasetSource> _sources = new Dictionary<Guid, DatasetSource>();
        private readonly IStorage _store;

        private volatile int _status = Status_NotStarted;

        public IdentityTally(IStorage store)
        {
            _store = store;
        }

        public void AddSource(DatasetSource source)
        {
            try
            {
                _sources.Add(source.Id, source);
            }
            catch (ArgumentException e)
            {
                throw new Exception($"There was an attempt to add data source with ID {source.Id} a second time", e);
            }
        }

        public async Task Process()
        {
            var status = Interlocked.CompareExchange(ref _status, Status_Running, Status_NotStarted);

            if (status == Status_NotStarted)
            {
                // Begin hashing rows into centralised storage

                var hashers = _sources.Select(kvp =>
                    new Hasher(kvp.Value, _store, _cancellationTokenSource.Token));

                var hashingTasks = hashers.Select(h => h.Process());

                await Task.WhenAll(hashingTasks);

                _status = Status_Completed;
            }
        }
    }
}