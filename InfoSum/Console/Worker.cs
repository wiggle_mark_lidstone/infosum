﻿using System.Linq;
using System.Threading.Tasks;
using InfoSum.DataSources;
using InfoSum.Processes;
using InfoSum.Storage;

namespace InfoSum.Console
{
    internal class Worker
    {
        private static readonly object OutputSyncRoot = new object();
        private readonly Configuration _config;
        private readonly DatasetSourceFactory _datasetSourceFactory;
        private readonly IdentityTally _engine;
        private readonly IStorage _storage;

        public Worker(Configuration config, DatasetSourceFactory datasetSourceFactory, IdentityTally engine,
            IStorage storage)
        {
            _datasetSourceFactory = datasetSourceFactory;
            _engine = engine;
            _storage = storage;
            _config = config;
        }

        public async Task StartAsync()
        {
            var sources = _config.GetFilePaths()
                .Select(fp => _datasetSourceFactory.CreateFileSource(fp))
                .ToArray();

            foreach (var source in sources) _engine.AddSource(source);

            await _engine.Process();

            foreach (var source in sources)
            {
                var total = _storage.GetTotalIdentityCountIn(source.Id)
                    .ContinueWith(t => Report($"All (valid) identity count in {source.Filename}: {t.Result}"));

                var distinct = _storage.GetDistinctIdentityCountIn(source.Id)
                    .ContinueWith(t => Report($"Distinct (valid) identity count in {source.Filename}: {t.Result}"));

                await Task.WhenAll(total, distinct);
            }

            if (sources.Length == 2)
            {
                var total = _storage.GetTotalOverlapBetween(sources[0].Id, sources[1].Id)
                    .ContinueWith(t =>
                        Report(
                            $"Total (valid) possible identity intersection between {sources[0].Id} and {sources[1].Id}: {t.Result}"));

                var distinct = _storage.GetDistinctOverlapBetween(sources[0].Id, sources[1].Id)
                    .ContinueWith(t =>
                        Report(
                            $"Distinct (valid) identity intersection between {sources[0].Id} and {sources[1].Id}: {t.Result}"));

                await Task.WhenAll(total, distinct);
            }
            
            Report("Processing complete.");
        }

        private void Report(string message)
        {
            // Simplest way to ensure only one thread is able to write to the console at once
            lock (OutputSyncRoot)
            {
                System.Console.WriteLine(message);
            }
        }
    }
}