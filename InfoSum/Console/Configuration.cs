﻿namespace InfoSum.Console
{
    public class Configuration
    {
        public string[] _paths;

        public Configuration(string[] args)
        {
            _paths = args;
        }

        public string[] GetFilePaths()
        {
            return _paths;
        }
    }
}