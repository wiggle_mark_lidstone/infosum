﻿using System.Threading.Tasks;
using InfoSum.DataSources;
using InfoSum.Processes;
using InfoSum.Storage;
using Microsoft.Extensions.DependencyInjection;

namespace InfoSum.Console
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .AddSingleton(new Configuration(args))
                .AddSingleton<Worker>()
                .AddSingleton<IdentityTally>()
                .AddSingleton<IStorage, CentralisedStorage>()
                .AddSingleton<DatasetSourceFactory>()
                .BuildServiceProvider();

            var worker = serviceProvider.GetService<Worker>();

            await worker.StartAsync();
        }
    }
}