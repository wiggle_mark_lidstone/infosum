﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace InfoSum.DataSources
{
    public class FileSource : DatasetSource, IDisposable
    {
        private readonly ILogger<FileSource> _logger;

        private readonly Regex _validator = new Regex("^[0-9]{8}$", RegexOptions.Compiled);

        private StreamReader _file;
        private long _lineCount = -0;

        public FileSource(string path, ILogger<FileSource> logger)
        {
            Filename = path;
            _logger = logger;
        }

        public string Filename { get; }

        public override Guid Id { get; } = Guid.NewGuid();

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public override async Task<IEnumerable<SensitiveRow>> GetKeyPage(int pageSize)
        {
            var page = new SensitiveRow[pageSize];
            var atEnd = false;
            int n;

            for (n = 0; n < pageSize && !atEnd; n++)
            {
                _lineCount++;
                string line;
                if ((line = (await _file.ReadLineAsync())?.Trim()) == null)
                {
                    atEnd = true;
                }
                else if (!_validator.IsMatch(line))
                {
                    _logger.LogWarning($"Line {_lineCount} of {Filename} ({line}) is not a valid record");
                    n--;
                }
                else
                {
                    page[n] = new SensitiveRow { Identifier = line, SourceReference = _lineCount };
                }
            }

            if (atEnd && n == 1)
            {
                return null;
            }

            return atEnd ? new ArraySegment<SensitiveRow>(page, 0, n - 1) : page;
        }

        public override Task<bool> ValidateSource()
        {
            // Simple implementation for files on disk
            return Task.Run(() =>
            {
                var context = $"while checking the validity of file source {Filename}";
                try
                {
                    if (!File.Exists(Filename))
                    {
                        _logger.LogInformation($"Path does not exist {context}");
                        return false;
                    }

                    using var fs = File.OpenRead(Filename);
                    if (!fs.CanRead)
                    {
                        _logger.LogInformation($"Cannot read file {context}");
                        return false;
                    }
                }
                catch (Exception e)
                {
                    _logger.LogWarning(e, $"Exception thrown {context}");
                    return false;
                }

                _file = File.OpenText(Filename);

                return true;
            });
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _file?.Dispose();
            }

            _file = null;
        }
    }
}