﻿using Microsoft.Extensions.Logging;

namespace InfoSum.DataSources
{
    public class DatasetSourceFactory
    {
        private readonly ILogger<FileSource> _fileSourceFileSourceLogger;

        public DatasetSourceFactory(ILogger<FileSource> fileSourceLogger)
        {
            _fileSourceFileSourceLogger = fileSourceLogger;
        }

        public FileSource CreateFileSource(string path)
        {
            return new FileSource(path, _fileSourceFileSourceLogger);
        }
    }
}