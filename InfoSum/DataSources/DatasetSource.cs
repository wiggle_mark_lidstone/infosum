﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InfoSum.DataSources
{
    public class SensitiveRow
    {
        public string Identifier { get; set; }
        public long SourceReference { get; set; }
    }

    public abstract class DatasetSource
    {
        /// <summary>
        ///     Some kind of unique identifier for this dataset
        /// </summary>
        public abstract Guid Id { get; }

        /// <summary>
        ///     Gets a page of keys to be processed
        /// </summary>
        /// <param name="pageSize">The page size (maximum number of records) to return</param>
        /// <returns></returns>
        public abstract Task<IEnumerable<SensitiveRow>> GetKeyPage(int pageSize);

        public abstract Task<bool> ValidateSource();
    }
}