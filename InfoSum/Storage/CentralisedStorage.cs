﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InfoSum.Storage
{
    // As Azure was mentioned - this could be a CosmosDb collection with the key used as
    //  the Cosmos partition key.  For now, however, it's represented by an in memory
    //  dictionary.
    public interface IStorage
    {
        Task Store(Guid sourceId, IEnumerable<AnonymisedRow> rows);

        Task<ulong> GetTotalIdentityCountIn(Guid source);

        Task<ulong> GetDistinctIdentityCountIn(Guid source);

        Task<ulong> GetTotalOverlapBetween(Guid sourceA, Guid sourceB);

        Task<ulong> GetDistinctOverlapBetween(Guid sourceA, Guid sourceB);
    }

    public class AnonymisedRow
    {
        public string Key { get; set; }
        public long SourceReference { get; set; }
    }

    public class CentralisedStorage : IStorage
    {
        // Dictionary Key = Identity Key, Dictionary Value = Set of sources containing said Identity Key
        private readonly ConcurrentDictionary<string, List<SourceRow>> _store =
            new ConcurrentDictionary<string, List<SourceRow>>();

        public Task Store(Guid sourceId, IEnumerable<AnonymisedRow> rows)
        {
            return Task.Run(() =>
            {
                foreach (var row in rows)
                {
                    var toStore = new SourceRow { AnonymisedRow = row, Source = sourceId };
                    _store.AddOrUpdate(row.Key,
                        k => new List<SourceRow> { toStore },
                        (k, set) =>
                        {
                            set.Add(toStore);
                            return set;
                        });
                }
            });
        }

        // By having had the partition key set to the Identity key, Cosmos would allow us to build queries
        //  that query all separate partitions concurrently and combine the results for us, while being
        //  easy to maintain and not requiring us to "reinvent the wheel" when it comes to the collation
        //  of data.
        public async Task<ulong> GetTotalIdentityCountIn(Guid source)
        {
            return (ulong) _store.Sum(kvp => kvp.Value.Count(sr => sr.Source == source));
        }

        public async Task<ulong> GetDistinctIdentityCountIn(Guid source)
        {
            return (ulong) _store.Count(kvp => kvp.Value.Any(sr => sr.Source == source));
        }

        public async Task<ulong> GetTotalOverlapBetween(Guid sourceA, Guid sourceB)
        {
            return (ulong) _store
                .Where(kvp => kvp.Value.Any(r => r.Source == sourceA) && kvp.Value.Any(r => r.Source == sourceB))
                .Sum(kvp => kvp.Value.Count(r => r.Source == sourceA) * kvp.Value.Count(r => r.Source == sourceB));
        }

        public async Task<ulong> GetDistinctOverlapBetween(Guid sourceA, Guid sourceB)
        {
            return (ulong) _store.Count(kvp =>
                kvp.Value.Any(sr => sr.Source == sourceA) && kvp.Value.Any(sr => sr.Source == sourceB));
        }

        private class SourceRow
        {
            public AnonymisedRow AnonymisedRow { get; set; }
            public Guid Source { get; set; }
        }
    }
}