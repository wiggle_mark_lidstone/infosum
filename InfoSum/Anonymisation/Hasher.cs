﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using InfoSum.DataSources;
using InfoSum.Storage;
using Org.BouncyCastle.Crypto.Digests;

namespace InfoSum.Anonymisation
{
    // Controls the process of taking a page of PII, hashing it and storing it in the passed storage mechanism
    public class Hasher
    {
        private const int PageSize = 10000;
        private readonly DatasetSource _source;
        private readonly IStorage _target;
        private readonly CancellationToken _token;

        public Hasher(DatasetSource source, IStorage target, in CancellationToken token)
        {
            _token = token;
            _target = target;
            _source = source;
        }

        public async Task Process()
        {
            if (_token.IsCancellationRequested)
            {
                return;
            }

            if (!await _source.ValidateSource())
            {
                throw new Exception($"Source {_source.Id} is not valid");
            }

            if (_token.IsCancellationRequested)
            {
                return;
            }

            // Paging shown here as a possible place to parallelise processing (could be farmed off to separate workers)
            IEnumerable<SensitiveRow> page = null;
            while ((page = await _source.GetKeyPage(PageSize)) != null)
            {
                if (_token.IsCancellationRequested)
                {
                    return;
                }

                var hashes = page.Select(pii =>
                {
                    // Assumption: that a 2^256 chance of collision of these keys is within an acceptable limit
                    var digest = new byte[512];
                    var digester = new Sha3Digest(512);
                    digester.BlockUpdate(Encoding.UTF8.GetBytes(pii.Identifier), 0, Encoding.UTF8.GetByteCount(pii.Identifier));
                    digester.DoFinal(digest, 0);

                    return new AnonymisedRow
                    {
                        Key = Convert.ToBase64String(digest),
                        SourceReference = pii.SourceReference
                    };
                });

                if (_token.IsCancellationRequested)
                {
                    return;
                }

                await _target.Store(_source.Id, hashes);

                if (_token.IsCancellationRequested)
                {
                    return;
                }
            }
        }
    }
}