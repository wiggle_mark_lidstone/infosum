# Instructions
These instructions assume a machine running Ubuntu 18.04.  Testing was performed on the standard Azure VM image, with the
source available in the folder ~/infosum.

1. Install the DotNet SDK using the Snap package manager:
    `sudo snap install dotnet-sdk --classic`
2. Build a self contained application:
    `dotnet publish -c Release --self-contained true --runtime linux-x64 <path to InfoSum.sln>`
3. Change to the publish folder (the "publish" folder mentioned at the end of the build output)
    `cd InfoSum/bin/Release/net5.0/linux-x64/publish`
4. Run the application, giving paths to the test files
    `./InfoSum <path to file A> <path to file B>`

# Assumed Requirements
1. There must be no reliance on an entire dataset being available at any time
2. Any data stored must be anonymised, with a mechanism to refer back to the original, un-anonymised data
3. The 1:2^256 chance of a collision with the SHA-3(512) hashing algorithm is an acceptably small probability of incorrectly identifying two UDPRNs as identical
4. Only rows with a valid UDPRN should be included in the "total" and "distinct" counts (8 numeric digits)
5. "Total overlap" refers to the maximum possible overlap (product of the duplicated items from both sets)

# Other Assumptions
I've assumed that the crux of the problem here is scaling out to massive data sets while preventing PII from passing between systems, so I've concentrated more on that than pretty code, testing or 

# Description of Program
This program has been designed to roughly map a simplistic scalable solution.  Scalability would rely upon the Hasher class managing separate hosts to process the sources in parallel, and store them in a scalable storage solution (such as Cosmos mentioned below).  The namespaces in the project roughly map to how I would expect the domains to be separated, with each class currently here representing an interface provided by that domain.  The exception is the "Console" namespace which contains the "plumbing" to make it all work.

I've mentioned Cosmos in the comments as I'm familiar with the Microsoft terminology and the possibility of moving to Azure was mentioned in the telephone call.  I'm assuming something similar will likely apply to all cloud database providers; in Cosmos, by chosing the key as the partition key, a query could act in parallel across all of the partitions, collecting up the "per source" results and compiling them, while allowing the query to be simple and human readable.

I've not included a true Cosmos implementation here as I've always had problems passing around code to create Azure resources without embedding my own account in scripts, and I've never created Azure resources from Linux.